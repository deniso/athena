# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( VP1MCPlugin )

# Install files from the package:
atlas_install_headers( VP1MCPlugin )

